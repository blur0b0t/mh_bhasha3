-- for streaming inserts
SELECT * FROM `mh-bhasha3.processed_data.pdata` order by id desc

--for visualization
with t as (
(SELECT * FROM `mh-bhasha3.processed_data.pdata` where exists ( select * from unnest(f_data.tags) as tags where tags IN ('train','staff','station','website','app','food')  )
limit 10)
UNION ALL
(SELECT * FROM `mh-bhasha3.processed_data.pdata` where not exists ( select * from unnest(f_data.tags) as tags where tags IN ('train')  ))
)
SELECT * FROM t
where f_data.priority in ('low','medium','high') 
and f_data.sentiment in ('positive','negative','neutral') 
-- and f_data.tags in ('train','staff','station','website','app','food')
and exists ( select * from unnest(f_data.tags) as tags where tags IN ('train','staff','station','website','app','food')  )
and not exists (( select * from unnest(f_data.tags) as tags where tags IN ('pnr')  )         )
order by id desc
