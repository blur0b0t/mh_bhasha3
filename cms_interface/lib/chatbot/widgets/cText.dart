

import 'dart:ui';

import 'package:flutter/material.dart';

Widget cText(String text,{Color color=Colors.greenAccent}){
  return

Chip(
            elevation: 20,
            padding: EdgeInsets.all(8),
            backgroundColor: color,
            shadowColor: Colors.black,
            // avatar: CircleAvatar(
            //   backgroundImage: NetworkImage(
            //       "https://pbs.twimg.com/profile_images/1304985167476523008/QNHrwL2q_400x400.jpg"), //NetworkImage
            // ), //CircleAvatar
            label: Text(
              text,
              style: TextStyle(fontSize: 20),
            ));
}
