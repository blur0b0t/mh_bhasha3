import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:mhs_pred_app/chatbot/chat_window.dart';

class LoadingText extends StatelessWidget {
  final String loadingText;
  const LoadingText({Key? key, required this.loadingText}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child:Container(
        alignment: Alignment.center,
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.all(Radius.circular(40))),
        height: swidth*0.22,
        width: swidth * 0.6,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            SpinKitDualRing(
              color: Colors.blue,
              size: 80.0,
            ),
            SizedBox(height: 15,),
            Text(
              loadingText,
              style: TextStyle(fontFamily: 'OpenSans',color: Colors.blueAccent,textBaseline: TextBaseline.alphabetic,fontSize: 30,  decoration: TextDecoration.none,
          decorationColor: Colors.yellow,),
            )
          ],
        )));
  }
}
