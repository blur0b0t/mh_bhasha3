
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:mhs_pred_app/chatbot/models/complaint_model.dart';

ComplaintModel complaint=ComplaintModel(
  timestamp: Timestamp.now(),
  f_data: {'tags': ['train'], 'priority': 'low', 'attributes': ['pnr=123456', 'scheduledat9pm'], 'sentiment': 'negative'},
  );