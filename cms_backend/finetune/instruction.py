categories=['train','station','suggestion','enquiry','staff','website']
instruction=f"""Identify the following from the complaint:
1)category of the complaint from the following categories:{categories.__str__()}.
2)priority of the complaint (low/medium/high).
3)attributes from the complaint.
4)sentiment of the complaint.
return the output in following format
#format start:
tags:[],priority:[],attributes:[],sentiment:[]
#format end
"""