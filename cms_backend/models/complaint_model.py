import time
class ComplaintModel(object):
    def __init__(self, id='', senderId='',lang='',complaintText='',audioURL='',audioText='',transText='',timestamp='',output='',ots='',f_data={}):
        self.id = id
        self.senderId = senderId
        self.lang = lang
        self.complaintText = complaintText
        self.audioURL = audioURL
        self.audioText = audioText
        self.transText = transText
        self.timestamp = timestamp
        self.output=output
        self.ots=ots
        self.f_data=f_data
    
    def from_dict(data):
        return ComplaintModel(
            id=data.get('id') if data.get('id')!=None else str(round(time.time() * 1000)),
            senderId=data.get('senderId'),
            lang=data.get('lang').lower(),
            complaintText=data.get('complaintText'),
            audioURL=data.get('audioURL'),
            audioText=data.get('audioText'),
            transText=data.get('transText'),
            timestamp=data.get('timestamp'),
            output=data.get('output'),
            ots=data.get('ots'),
            f_data=data.get('f_data')
            )
    